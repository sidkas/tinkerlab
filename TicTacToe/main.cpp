#include <iostream>
#include <stdio.h>
#include <ctype.h>  
#include <map>
#include <string>

#include "include/display.hpp"
#include "include/helpers.hpp"

std::map<int, std::string> spots { 
    {1, "1"}, {2, "2"}, {3, "3"}, 
    {4, "4"},{5, "5"},{6, "6"},
    {7, "7"},{8, "8"},{9, "9"}
};

std::string playerTags[] = {"X", "O"};

std::string getCurrentPlayerTag(int turn ) {
    if (turn%2 == 0) return playerTags[0];
    else return playerTags[1];
}

bool arrayContains( std::string iterator[], std::string value) {

    int len = 2;
    for (int i=0; i< len;i++) {
        if (iterator[i] == value) {
            return true;
        }
    }

    return false;
}

// States:
bool playing = false; 
bool complete = false;
int turn = 0;

bool mapContains(std::map<int, std::string> spots, int input) {
    
    for (const auto& spot: spots) {
        if (spot.first == input) {
            return true;
        }
    }

    return false;
}   


bool winCondition(std::map<int, std::string> spots) {
    // Horizontal cases
    if(
        (spots[1] == spots[2]) && (spots[2] == spots[3]) || \
        (spots[4] == spots[5]) && (spots[5] == spots[6] ) || \
        (spots[7] == spots[8]) && (spots[8] == spots[9] )
    ) 
    {
        return true;
    }

    // Vertical cases
    if(
        (spots[1] == spots[4]) && (spots[4] == spots[7]) || \
        (spots[2] == spots[5]) && (spots[5] == spots[8] ) || \
        (spots[3] == spots[6]) && (spots[6] == spots[9] )
    ) 
    {
        return true;
    }

    // Diagonal cases
    if(
        (spots[1] == spots[5]) && (spots[5] == spots[9]) || \
        (spots[3] == spots[5]) && (spots[5] == spots[7])
    ) 
    {
        return true;
    }

    return false;
}



int main() {

    clearScreen();

    dispBanner();

    // Ask user to start the game
    std::cout << "Press q to quit or any key to continue..."<< std::endl;
    char input = getUserInput(); clearScreen();
    std::cout << &input << std::endl;
    if (input == 'q' || input == 'Q' ) exit(EXIT_SUCCESS);
    else playing=true;

    int prev_turn = -1;

    while (playing) {
        clearScreen();
        drawBoard(spots);

        if (prev_turn == turn) {
            std::cout << "Invalid spot selected, pick another." << std::endl;
        }
        prev_turn = turn;

        std::cout << "Player " \
        + std::to_string(turn %2 + 1) + "'s turn: Pick your spot of press q to quit" << std::endl;
        

        input = getUserInput();
        std::cout << input << std::endl;
        std::cout << isdigit(input) << std::endl;
        if ( input == 'q' || input == 'Q' ) {
            playing=false;
        } else if ( isdigit((int)input) ) {

            int temp = input - '0';

            if (mapContains(spots, temp)) {
                if ( !arrayContains(playerTags, spots[temp])) {
                    turn += 1;
                    spots[temp] = getCurrentPlayerTag(turn);
                }
            }
            
        }
        
        if (winCondition(spots)) {
            playing = false;
            complete = true;
        }
   

        if (turn>8) playing = false;


    }

    clearScreen();
    drawBoard(spots);

    if (complete) {
        std::string tag = getCurrentPlayerTag(turn);

        if (tag == playerTags[1]) {
            std::cout << "Player 1 Wins!" << std::endl;
        } else {
            std::cout << "Player 2 Wins!" << std::endl;
        }
    } else {
        std::cout << "It's draw!" << std::endl;
    }



    
 }


