#pragma once

#include <iostream>
#include <map>
#include <string>


std::string reset = "\033[0m";

void dispBanner()
{ 
    std::cout << "\nWelcome to Tic Tac Toe!!" << std::endl;
    std ::cout << R"(
████████╗██╗ ██████╗    ████████╗ █████╗  ██████╗    ████████╗ ██████╗ ███████╗
╚══██╔══╝██║██╔════╝    ╚══██╔══╝██╔══██╗██╔════╝    ╚══██╔══╝██╔═══██╗██╔════╝
   ██║   ██║██║            ██║   ███████║██║            ██║   ██║   ██║█████╗  
   ██║   ██║██║            ██║   ██╔══██║██║            ██║   ██║   ██║██╔══╝  
   ██║   ██║╚██████╗       ██║   ██║  ██║╚██████╗       ██║   ╚██████╔╝███████╗
   ╚═╝   ╚═╝ ╚═════╝       ╚═╝   ╚═╝  ╚═╝ ╚═════╝       ╚═╝    ╚═════╝ ╚══════╝
                                                                               )";
    std::cout << reset << std::endl;
}


void drawBoard(std::map<int, std::string> spots) {
  
    std::string board =  "|"+ spots[1]+"|"+ spots[2]+"|"+ spots[3]+"|\n" + \
                         "|"+ spots[4]+"|"+ spots[5]+"|"+ spots[6]+"|\n" + \
                         "|"+ spots[7]+"|"+ spots[8]+"|"+ spots[9]+"|\n";

    std::cout << board << std::endl;

}
