#pragma once

#include <iostream> 
#include <string>

std::string getOsName()
{
    #ifdef _WIN32
    return "win";
    #elif _WIN64
    return "win";
    #elif __APPLE__ || __MACH__
    return "mac";
    #elif __linux__
    return "Linux";
    #elif __FreeBSD__
    return "FreeBSD";
    #elif __unix || __unix__
    return "Unix";
    #else
    return "Other";
    #endif
}  

void clearScreen() {
    std::string osName = getOsName();

    if (osName == "win") {
        system("cls");
    } else {
        system("clear");
    }
}

char getUserInput() {
    system("stty raw"); 
    char input = getchar(); // Get user input from the keyboard
    system("stty cooked");
    return input;
}
